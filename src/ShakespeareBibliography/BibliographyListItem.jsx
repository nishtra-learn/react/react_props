import React from "react";

export default function BibliographyListItem(props) {
    let { title, yearOfPublication: year } = props.book;
    return (
        <li>{title} ({year})</li>
    );
}