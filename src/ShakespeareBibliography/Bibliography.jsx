import React from "react";
import { Book } from "./Book";
import BibliographyListItem from "./BibliographyListItem";
import BookDescription from "./BookDescription";

export default function Bibliography() {
    let tragedies = [
        new Book('Ромео и Джульетта', 1597),
        new Book('Юлий Цезарь', 1599),
        new Book('Гамлет', 1603),
        new Book('Отелло', 1604),
        new Book('Король Лир', 1608),
        new Book('Макбет', 1623),
        new Book('Антоний и Клеопатра', 1607),
        new Book('Кориолан', 1623),
        new Book('Троил и Крессида', 1609),
        new Book('Тимон Афинский', 1623)
    ];
    tragedies[0].description = `«Траги́ческая исто́рия о Га́млете, при́нце да́тском» (англ. The Tragical Historie of Hamlet, Prince of Denmarke) или просто 
    «Га́млет» — трагедия Уильяма Шекспира в пяти актах, одна из самых известных его пьес и одна из самых знаменитых пьес в мировой драматургии. 
    Написана в 1600—1601 годах. Это самая длинная пьеса Шекспира — в ней 4042 строки и 29 551 слово.
    Наиболее вероятная дата сочинения и первой постановки — 1600—1601 годы (театр «Глобус», Лондон). 
    Первый исполнитель заглавной роли — Ричард Бёрбедж; Шекспир играл тень отца Гамлета.
    Трагедия входит во Всемирную библиотеку (список наиболее значимых произведений мировой литературы Норвежского книжного клуба).`

    return (
        <React.Fragment>
            <ol>
                {
                    tragedies.map(item => {
                        return <BibliographyListItem key={item.title} book={item}></BibliographyListItem>
                    })
                }
            </ol>
            <hr/>
            <BookDescription book={tragedies[0]}></BookDescription>
        </React.Fragment>
    );
}