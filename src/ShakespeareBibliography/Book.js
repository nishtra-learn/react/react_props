export class Book {
    title;
    yearOfPublication;
    description;

    constructor (title, yearOfPublication, description) {
        this.title = title;
        this.yearOfPublication = yearOfPublication;
        this.description = description ?? '';
    }
}