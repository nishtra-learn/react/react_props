import React from "react"

export default function BookDescription(props) {
    let book = props.book;

    return (
        <div className="BookDescription">
            <h2>{book.title} ({book.yearOfPublication})</h2>
            <div>{book.description}</div>
        </div>
    );
}