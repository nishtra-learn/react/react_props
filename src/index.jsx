import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Bibliography from './ShakespeareBibliography/Bibliography';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Movie from './FavoriteMovie/Movie';
import PersonalInfoPage from './PersonalInfoPage/PersonalInfoPage';
import { PET_MOCK } from './Pet/PetMock';
import Pet from './Pet/Pet';

ReactDOM.render(
  <React.StrictMode>
    <div className="container">
      <Bibliography></Bibliography>
      <hr/>
      <Movie></Movie>
      <hr/>
      <PersonalInfoPage></PersonalInfoPage>
      <hr/>
      <Pet name={PET_MOCK.name} photo={PET_MOCK.photo}></Pet>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
