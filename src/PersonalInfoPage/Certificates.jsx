import React from 'react'
import './Certificates.css'

export default function Certificates(props) {
    let data = props.data;

    return (
        <div className="Certificates">
            <div className="d-flex">
                {
                    data.map(item => {
                        return <span class="badge badge-warning">{item}</span>
                    })
                }
            </div>
        </div>
    );
}