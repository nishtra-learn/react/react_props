import React from 'react'

export default function Photo(props) {
    let src = props.src;

    return (
        <figure>
            <img src={src} alt="userpic" className="figure-img img-fluid"/>
        </figure>
    );
}