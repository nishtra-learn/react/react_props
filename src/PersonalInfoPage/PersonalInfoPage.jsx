import React from 'react';
import { CV_INFO_MOCK } from "./shared/cvInfoMock";
import Photo from "./Photo";
import Skills from './Skills';
import Certificates from './Certificates';
import './PersonalInfoPage.css';

export default function PersonalInfoPage() {
    return (
        <React.Fragment>
            <h3>Personal Info</h3>
            <div className="PersonalInfoPage row">
                <div className="col-md-3">
                    <Photo src={CV_INFO_MOCK.profilePicUrl}></Photo>
                </div>
                <div className="col-md-9">
                    <div className="row">
                        <div className="col-md-2">First name</div>
                        <div className="col-md-10">{CV_INFO_MOCK.firstName}</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Last name</div>
                        <div className="col-md-10">{CV_INFO_MOCK.lastName}</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Phone number</div>
                        <div className="col-md-10">{CV_INFO_MOCK.phoneNumbers}</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Skills</div>
                        <div className="col-md-10">
                            <Skills data={CV_INFO_MOCK.skills}></Skills>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Certificates</div>
                        <div className="col-md-10">
                            <Certificates data={CV_INFO_MOCK.certificates}></Certificates>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}