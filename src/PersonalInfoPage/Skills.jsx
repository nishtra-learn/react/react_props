import React from 'react'
import './Skills.css'

export default function Skills(props) {
    let data = props.data;

    return (
        <div className="Skills">
            <div className="d-flex">
                {
                    data.map(item => {
                        return <span class="badge badge-success">{item}</span>
                    })
                }
            </div>
        </div>
    );
}