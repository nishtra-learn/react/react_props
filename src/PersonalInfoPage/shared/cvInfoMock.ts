import { CvModel } from "./cvModel";
import { LinkModel } from './linkModel';

export const CV_INFO_MOCK = new CvModel();
CV_INFO_MOCK.firstName = 'Yuri';
CV_INFO_MOCK.lastName = 'Kosse';
CV_INFO_MOCK.phoneNumbers = ['+380971234567'];
CV_INFO_MOCK.address = 'Ukraine, Kryvyi Rih';
CV_INFO_MOCK.profilePicUrl = 'https://www.timeshighereducation.com/sites/default/files/byline_photos/anonymous-user-gravatar_0.png'
CV_INFO_MOCK.dateOfBirth = new Date(1993, 1, 2);
CV_INFO_MOCK.workExperience = ['English tutor', 'Copywriter'];
CV_INFO_MOCK.skills = ['C++', 'C#', 'WPF', 'ASP.NET', 'HTML', 'CSS', 'JS', 'Node.js'];
CV_INFO_MOCK.certificates = ['Microsoft Azure'];
CV_INFO_MOCK.links = [new LinkModel('https://gitlab.com/Nishtra', 'Gitlab')];
CV_INFO_MOCK.desiredSalary = 9999;