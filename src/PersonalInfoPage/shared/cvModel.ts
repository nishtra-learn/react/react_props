import { LinkModel } from "./linkModel";

export class CvModel {
    firstName: string;
    lastName: string;
    phoneNumbers: string[] = [];
    address: string;
    profilePicUrl: string;
    dateOfBirth: Date = new Date();
    desiredSalary: number;

    workExperience: string[] = [];

    skills: string[] = [];
    certificates: string[] = [];

    links: LinkModel[] = [];
}