export class LinkModel {
    href: string = '#';
    text: string = null;

    constructor(href: string, text?: string) {
        this.href = href;
        if (text)
            this.text = text;
    }

    getText(): string {
        return this.text ?? this.href;
    }
}