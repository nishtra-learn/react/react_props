import React from 'react';

export default function PetPhoto(props) {
    let src = props.src;

    return (
        <figure className="figure">
            <img src={src} className="figure-img img-fluid" alt=""/>
        </figure>
    );
}