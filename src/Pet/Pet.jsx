import React from 'react';
import PetPhoto from './PetPhoto';

export default function Pet(props) {
    let {name, photo} = props;

    return (
        <div className="Pet">
            <h3>{name}</h3>
            <PetPhoto src={photo}></PetPhoto>
        </div>
    );
}