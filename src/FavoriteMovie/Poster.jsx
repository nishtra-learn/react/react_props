import React from 'react'

export default function Poster(props) {
    let src = props.src;

    return (
        <figure>
            <img src={src} alt="poster" className="figure-img img-fluid"/>
        </figure>
    );
}