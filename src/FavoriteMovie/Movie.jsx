import React from 'react';
import Poster from './Poster';

export default function Movie() {
    let movieMock = {
        title: 'Inception',
        year: 2010,
        director: 'Christopher Nolan',
        cast: [
            'Leonardo DiCaprio',
            'Joseph Gordon-Levitt',
            'Ellen Page',
            'Tom Hardy',
            'Ken Watanabe',
            'Dileep Rao',
            'Cillian Murphy',
            'Tom Berenger',
            'Marion Cotillard',
            'Pete Postlethwaite',
            'Michael Caine',
            'Lukas Haas'
        ],
        poster: 'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg'
    };

    return (
        <div className="Movie row">
            <div className="col-md-4">
                <Poster src={movieMock.poster}></Poster>
            </div>
            <div className="col-md-8">
                <h2>{movieMock.title} ({movieMock.year})</h2>
                <div className="row">
                    <div className="col-md-2">Director</div>
                    <div className="col-md-10">{movieMock.director}</div>
                </div>
                <div className="row">
                    <div className="col-md-2">Cast</div>
                    <div className="col-md-10">
                        <ul className="list-unstyled">
                            {
                                movieMock.cast.map(actor => {
                                    return <li>{actor}</li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}